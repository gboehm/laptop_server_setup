#!/bin/bash
torrentid=$1
torrentname=$2
torrentpath=$3

dl=$(deluge-console -U 'yoro' -P 'pn&r6Kh9q%DNSu4nX49U' "info -v $torrentid" 2>&1 | grep -P "Download Folder: .*" -o | grep -oP "/.*")

if [[ $dl =~ "media" ]]; then
    deluge-console -U 'yoro' -P 'pn&r6Kh9q%DNSu4nX49U' "rm -c $torrentid"
fi

