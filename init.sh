#!/bin/bash

#### super setup to avoid timeouts
super() {
    echo $pass | sudo -S -p '' $@
}

while [ -z $supered ]
do
    echo -n 'Enter user password : '
    stty -echo
    read pass
    stty echo
    echo ''
    if super true 2>/dev/null; then
        supered=true
    else
        echo -n 'Wrong password. '
    fi
done
echo -n 'Enter deluge user password to use : '
read delugePassword
echo -n 'Enter flexget password to use : '
read flexgetPassword
echo -n 'Enter nordvpn username : '
read nordUser
echo -n 'Enter nordvpn password : '
read nordPass

super dpkg --configure -a || exit
super apt update && super apt upgrade -y || exit

#? quality of life stuff
super apt install -y zsh neovim fzf pip python-is-python3 bat || exit
cd ~
git clone https://github.com/guillaumeboehm/linux_new_install || exit
cd linux_new_install
git checkout wm

#zsh
cd ~
# sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
# cd ~/.oh-my-zsh
# while read plug; do
#     list=($plug)
#     url=${list[0]}
#     path=${list[1]}
#     echo "add $url to custom/plugins/$path"
#     git submodule add -f $url custom/plugins/$path
# done <<< $(cat ~/linux_new_install/.omz_plugins)

mkdir -p $HOME/.config/zsh
export ZSH_HOME=$HOME/.config/zsh
export ZPLUG_HOME=$ZSH_HOME/zplug
git clone https://github.com/zplug/zplug $ZPLUG_HOME

cp -r ~/laptop_server_setup/.zshrc ~ || exit
super chsh -s /bin/zsh yoro || exit

#golang needed for hexokinase
super apt install golang-go -y
# super true # just to use sudo on the next lines
# wget -c https://golang.org/dl/go1.17.2.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
#nvim
cd ~
mkdir -p .config || exit
cp -r ~/linux_new_install/.config/nvim/ ~/.config/ || exit
cd ~/.config/nvim || exit
./update.sh || exit
cd ~

#gitconfig
cp ~/laptop_server_setup/.gitconfig ~ || exit

#laptop lid config
super sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf || exit
super sed -i 's/#LidSwitchIgnoreInhibited=yes/LidSwitchIgnoreInhibited=no/' /etc/systemd/logind.conf || exit
super apt install acpi-support vbetool -y || exit
super true
sudo bash -c 'echo "event=button/lid.*
action=/etc/acpi/lid.sh" > /etc/acpi/events/lid-button' || exit
sudo bash -c 'echo "#!/bin/bash

grep -q close /proc/acpi/button/lid/*/state

if [ $? = 0 ]; then
    sleep 0.2 && vbetool dpms off
fi

grep -q open /proc/acpi/button/lid/*/state

if [ $? = 0 ]; then
    vbetool dpms on
fi" > /etc/acpi/lid.sh' || exit
super chmod +x /etc/acpi/lid.sh || exit

#? node stuff
super apt install -y npm || exit
super npm install -g n@latest
super n lts

#? tmux stuff
cp -r ~/linux_new_install/.tmux.conf ~/
cp -r ~/linux_new_install/.tmux/ ~/ #copies the plugin manager

#? nginx stuff
super apt install -y nginx || exit
super systemctl enable nginx || exit
super systemctl start nginx || exit
super cp -r ~/laptop_server_setup/sites-available /etc/nginx
super ln -sf /etc/nginx/sites-available/* /etc/nginx/sites-enabled/

#? vpn
super true
sh <(wget -qO - https://downloads.nordcdn.com/apps/linux/install.sh)
super nordvpn login --username "$nordUser" --password "$nordPass" || exit
super nordvpn whitelist add 22 || exit
super nordvpn whitelist add 80 || exit
super nordvpn whitelist add 58846 || exit
super nordvpn autoconnect on || exit
super nordvpn connect || exit
super systemctl enable nordvpn || exit
super systemctl start nordvpn || exit

#? media server
wget https://github.com/MediaBrowser/Emby.Releases/releases/download/4.6.4.0/emby-server-deb_4.6.4.0_amd64.deb -O emby-server.deb || exit
super dpkg -i emby-server.deb || exit
rm emby-server.deb || exit

#? BitTorrent client
pip install setuptools || exit
super apt install deluged deluge-web deluge-console -y || exit
super adduser --system  --gecos "Deluge Service" --disabled-password --group --home /var/lib/deluge deluge || exit
super adduser yoro deluge || exit
super adduser emby deluge || exit
super cp deluged.service /etc/systemd/system/ || exit
super systemctl enable /etc/systemd/system/deluged.service || exit
super systemctl start deluged || exit
super cp deluge-web.service /etc/systemd/system/ || exit
super systemctl enable /etc/systemd/system/deluge-web.service || exit
super systemctl start deluge-web || exit
super mkdir -p /var/log/deluge || exit
super chown -R deluge:deluge /var/log/deluge || exit
super chmod -R 750 /var/log/deluge || exit
super cp deluge /etc/logrotate.d/deluge || exit
super true
sudo bash -c "echo \"yoro:$delugePassword:10\" >> /var/lib/deluge/.config/deluge/auth"
#dark mode
super mv /usr/lib/python3/dist-packages/deluge/ui/web/icons/ /usr/lib/python3/dist-packages/deluge/ui/web/icons.bak & sudo mv /usr/lib/python3/dist-packages/deluge/ui/web/css/deluge.css /usr/lib/python3/dist-packages/deluge/ui/web/css/deluge.css.bak || exit # backup
super wget -c https://github.com/joelacus/deluge-web-dark-theme/raw/main/deluge_web_dark_theme.tar.gz -O - | sudo tar -xz -C /usr/lib/python3/dist-packages/deluge/ui/web/ || exit
super sed -i 's/"theme": "gray"/"theme": "dark"/' /var/lib/deluge/.config/deluge/web.conf || exit
#/var path
super ln -sf /var/lib/deluge/.config/deluge ~/.config/deluge/varpath
#autoRemovePlus
super wget https://forum.deluge-torrent.org/download/file.php?id=6597&sid=f69f2925eaaf7582ebc24c18f61af07f -O /var/lib/deluge/.config/deluge/AutoRemovePlus-2.0.0-py3.8.egg
mkdir -p downloads
sudo chmod 777 downloads
mkdir -p media
sudo chmod 777 media
mkdir -p torrents
sudo chmod 777 torrents
mkdir -p seeds
sudo chmod 777 seeds
cp torrent_downloaded.sh ~/ || exit

#? auto dl stuff
super apt install python3-venv python3-testresources -y || exit
pip install flexget || exit
pip install deluge-client || exit
flexget web passwd $flexgetPassword || exit
super cp flexget.service /etc/systemd/system/ || exit
super systemctl enable /etc/systemd/system/flexget.service || exit
super systemctl start flexget || exit
cp config.yml ~/.config/flexget/ || exit
cp variables.yml ~/.config/flexget/ || exit

#? netdata stuff
bash <(curl -Ss https://my-netdata.io/kickstart.sh) --stable-channel --disable-telemetry
sudo sed -i 's/# allow dashboard from = localhost */allow dashboard from = localhost 192.168.*/' /etc/netdata/netdata.conf || exit
sudo ufw allow 19999 || exit

#? security stuff
super ufw default deny || exit
super ufw allow 22/tcp || exit
super ufw allow 80 || exit
super ufw allow 58846 || exit #deluged
super ufw enable || exit
super ufw reload || exit

#? Follow up stuff
cp ~/laptop_server_setup/TODO_AFTER_INSTALL ~/

#? cleanup
super apt autoremove -y || exit
rm -rf ~/linux_new_install || exit
rm -rf ~/laptop_server_setup || exit
